INSTALLATION
------------

### Install
````
$ git clone https://most2894@bitbucket.org/most2894/yii2-socket-example.git
$ composer install
````

CONFIGURATION
-------------

### Components
Edit the file `config/console.php` with real data, for example:
- Socket
```php
'socket' => [
    'class' => 'app\components\Socket',
    'address' => '127.0.0.1',
    'port' => 8080,
],
```
- GeoHelper
```php
'geohelper' => [
    'class' => 'app\components\GeoHelper',
    'apiKey' => 'LbGX5iGEJmXBuqBrErdQmim4KA1RVdbi',
    'locale' => [
        'lang' => 'ru'
    ],
],
```
### Console command
Start daemon
```
$ php yii socket/start
```