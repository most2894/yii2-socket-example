var app = new Vue({
    el: '#app',
    data: {
        ws: null,
        currentSort:'name',
        currentSortDir:'asc',
        alert: {
          status: false,
          'message': ''
        },
        params: {
            filter: {
                'name': ''
            },
            pagination: {
                page: 1,
                limit: 20
            },
            order: {
                'by': 'name',
                'order': 'asc'
            }
        },
        response: {
            result: []
        }
    },
    computed: {
        requestName: {
            get(){
                return this.params.filter.name;
            },
            set(value){
                this.params.filter.name = value;
                app.ws.send(JSON.stringify({
                    'method': 'country',
                    'data': this.params
                }));
            }
        },
        sortedItems:function() {
            return this.response.result.sort((a,b) => {
                let modifier = 1;
                if(this.currentSortDir === 'desc') modifier = -1;
                if(a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
                if(a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
                return 0;
            });
        }
    },
    created: function() {
        let self = this;
        this.ws = new WebSocket('ws://localhost:8080');
        this.ws.onopen = function() {
            self.alert = {
                'status': true,
                'message': 'установлено'
            };
            app.ws.send(JSON.stringify({
                'method': 'country',
                'data': self.params
            }));
        };
        this.ws.onclose = function(e) {
            self.alert = {
                'status': false,
                'message': 'закрыто'
            };
        };
        this.ws.onmessage = function(msg) {
            let message = JSON.parse(msg.data);
            switch (message.method) {
                case 'connected':
                    self.alert = {
                        'status': true,
                        'message': message.data.message
                    };
                    break;
                case 'country':
                    self.response = message.data;
                    break;
                default:
                    break;
            }
        }
    },
    methods:{
        sort:function(s) {
            //if s == current sort, reverse
            if(s === this.currentSort) {
                this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
            }
            this.currentSort = s;
        }
    }
});


