<?php


namespace app\components;


use Exception;
use yii\base\Component;
use yii\httpclient\Client;

class GeoHelper extends Component
{

    /** @var string */
    public $apiKey;

    /** @var array */
    public $locale;

    /** @var Client */
    protected $client;

    /**
     * @throws Exception
     */
    public function init()
    {
        if (!$this->apiKey) {
            throw new Exception('parameter $apiKey empty');
        }

        if (!$this->locale) {
            throw new Exception('parameter $locale empty');
        }

        $this->client = new Client(['baseUrl' => "http://geohelper.info/api/v1/"]);

    }

    /**
     * @param array $filter
     * @param array $pagination
     * @param array $order
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function countries($filter = [], $pagination = [], $order =[])
    {
        return $this->httpRequest('countries', [
            'filter' => $filter,
            'pagination' => $pagination,
            'order' => $order,
        ]);
    }

    /**
     * @param $url
     * @param array $params
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected function httpRequest($url, $params = [])
    {
        $response = $this->client->createRequest()
            ->setUrl($url)
            ->setData([
                'apiKey' => $this->apiKey,
                'locale' => $this->locale,
            ])
            ->addData($params)->send();

        if($response->isOk) {
            return json_decode($response->content);
        }

        throw new \yii\httpclient\Exception($response->content, $response->getStatusCode());
    }

}