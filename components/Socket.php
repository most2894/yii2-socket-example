<?php


namespace app\components;


use app\events\ExceptionEvent;
use app\events\socket\ClientErrorEvent;
use app\events\socket\ClientEvent;
use app\events\socket\ClientMessageEvent;
use Ratchet\App;
use Ratchet\ConnectionInterface;
use yii\base\Component;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\MessageComponentInterface;

class Socket extends Component implements MessageComponentInterface
{

    const SOCKET_OPEN = 'open';

    const SOCKET_CLOSE = 'close';

    const SOCKET_ERROR = 'error';

    const SOCKET_CLIENT_CONNECTED = 'client_connected';

    const SOCKET_CLIENT_DISCONNECTED = 'client_disconnected';

    const SOCKET_CLIENT_MESSAGE = 'client_connected_message';

    const SOCKET_CLIENT_ERROR = 'client_connected_error';

    public $address;

    public $port;

    /**
     * @var App $server
     */
    public $server;

    protected $clients = null;

    /**
     * @throws \Exception
     */
    public function init()
    {

        if(!$this->address) {
            throw new \Exception("address parameter is empty");
        }

        if(!$this->port) {
            throw new \Exception("port parameter is empty");
        }

        try {
            $this->server = IoServer::factory(
                new HttpServer(
                    new WsServer(
                        $this
                    )
                ),
                $this->port,
                $this->address
            );
            $this->clients = new \SplObjectStorage();
        } catch (\Exception $e) {
            $this->trigger(self::SOCKET_ERROR, new ExceptionEvent([
                'exception' => $e
            ]));
        }
    }

    public function start()
    {
        $this->trigger(self::SOCKET_OPEN);
        $this->server->run();
    }

    public function stop()
    {
        $this->server->socket->shutdown();
        $this->trigger(self::SOCKET_CLOSE);
    }

    /**
     * @param ConnectionInterface $conn
     *
     * @event WSClientEvent EVENT_CLIENT_CONNECTED
     */
    function onOpen(ConnectionInterface $conn)
    {
        $this->trigger(self::SOCKET_CLIENT_CONNECTED, new ClientEvent([
            'client' => $conn
        ]));
        $this->clients->attach($conn);
    }

    /**
     * @param ConnectionInterface $conn
     *
     * @event WSClientEvent EVENT_CLIENT_DISCONNECTED
     */
    function onClose(ConnectionInterface $conn)
    {
        $this->trigger(self::SOCKET_CLIENT_DISCONNECTED, new ClientEvent([
            'client' => $conn
        ]));
        $this->clients->detach($conn);
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     *
     * @event WSClientErrorEvent EVENT_CLIENT_ERROR
     */
    function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->trigger(self::SOCKET_CLIENT_ERROR, new ClientErrorEvent([
            'client' => $conn,
            'exception' => $e
        ]));
//        if ($this->closeConnectionOnError) {
//            $conn->close();
//        }
    }

    function onMessage(ConnectionInterface $client, $message)
    {
        $this->trigger(self::SOCKET_CLIENT_MESSAGE, new ClientMessageEvent([
            'client' => $client,
            'message' => $message
        ]));
    }
}