<?php


namespace app\commands;


use app\components\Socket;
use app\events\ExceptionEvent;
use app\events\socket\ClientEvent;
use app\events\socket\ClientMessageEvent;
use Ratchet\ConnectionInterface;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\httpclient\Exception;

class SocketController extends Controller
{

    public function actionStart()
    {
        $socket = Yii::$app->socket;

        $socket->on(Socket::SOCKET_OPEN, function($e) use($socket) {
            $this->stdout("[I] server started at port {$socket->port} \n");
        });

        $socket->on(Socket::SOCKET_ERROR, function (ExceptionEvent $e) {
            $this->stderr("[E] file: {$e->exception->getFile()}, line: {$e->exception->getLine()} message: {$e->exception->getMessage()} \n");
        });

        $socket->on(Socket::SOCKET_CLIENT_CONNECTED, function (ClientEvent $e) {
            $this->stdout("[I] client ({$e->client->resourceId}) connected  \n");
            $e->client->send(json_encode([
                'method' => 'connected',
                'data' => [
                    'message' => "установлено id:({$e->client->resourceId})"
                ]
            ]));
        });

        $socket->on(Socket::SOCKET_CLIENT_DISCONNECTED, function (ClientEvent $e) {
            $this->stderr("[I] client ({$e->client->resourceId}) disconnected  \n");
        });

        $socket->on(Socket::SOCKET_CLIENT_MESSAGE, function (ClientMessageEvent $e) {
            try {
                $message = json_decode($e->message);
                $this->stdout("[I] start method {$message->method} \n");
                call_user_func(array($this, 'method' . ucfirst($message->method)), $e->client, $message->data);
            } catch (\Exception $e) {
                $this->stderr("[E] file: {$e->getFile()}, line: {$e->getLine()} message: {$e->getMessage()} \n");
            }
        });

        $socket->start();
    }

    /**
     * @param ConnectionInterface $client
     * @param $data
     */
    public function methodCountry($client, $data)
    {
        try {
            $client->send(json_encode([
                'method' => 'country',
                'data' => Yii::$app->geohelper->countries($data->filter, $data->pagination, $data->order)
            ]));
        } catch (InvalidConfigException $e) {
            $this->stderr("[E] file: {$e->getFile()}, line: {$e->getLine()} message: {$e->getMessage()} \n");
        } catch (Exception $e) {
            $this->stderr("[E] file: {$e->getFile()}, line: {$e->getLine()} message: {$e->getMessage()} \n");
        }
    }

}