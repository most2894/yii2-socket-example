<?php


namespace app\events\socket;


use yii\base\Event;

class ClientMessageEvent extends Event
{

    /**
     * @var \Ratchet\ConnectionInterface $client
     */
    public $client;


    /**
     * @var string $message
     */
    public $message;

}