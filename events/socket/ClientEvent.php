<?php


namespace app\events\socket;


use yii\base\Event;

class ClientEvent extends Event
{
    /**
     * @var \Ratchet\ConnectionInterface $client
     */
    public $client;
}