<?php


namespace app\events\socket;


use yii\base\Event;

class ClientErrorEvent extends Event
{
    /**
     * @var \Ratchet\ConnectionInterface $client
     */
    public $client;

    /**
     * @var \Exception $exception
     */
    public $exception;

}