<?php


namespace app\events;


use yii\base\Event;

class ExceptionEvent extends Event
{
    /**
     * @var \Exception
     */
    public $exception;
}