<?php

/* @var $this yii\web\View */

$this->title = 'Socket';

?>

<div id="app">


    <div class="alert" v-bind:class="[ alert.status ? 'alert-success' : 'alert-danger']">
        <b>Статус соединения:</b> {{ alert.message }}
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Фильтр</h2>
        </div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-lg-1 control-label">Название</label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" v-model="requestName">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default" id="socket">
        <div class="panel-heading">
            <h2 class="panel-title">Список стран</h2>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th><a href="#" @click="sort('name')">Название</a></th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="item in sortedItems">
                <td>{{item.name}}</td>
            </tr>
            </tbody>
        </table>
    </div>

</div>
